var base_url = 'http://localhost:8080/api/';
var USERNAME = "tom";
var PASSWORD = "123";
var auth;
var queryList = [];

var mainDiv = document.querySelector('#contentDiv');

function renderAuthForm(){
  mainDiv.innerHTML = "<div class=\"card\">\n"
      + "  <div class=\"card-body\">\n"
      + "<h1>Вход</h1>"
      + "<input type=\"text\" id=\"usernameInput\" class=\"form-control\" aria-label=\"login\" placeholder='login' style='margin: 20px; padding: 10px' aria-describedby=\"inputGroup-sizing-default\">"
      + "<input type=\"password\" id=\"passwordInput\" class=\"form-control\" aria-label=\"password\" placeholder='password' style='margin: 20px; padding: 10px' aria-describedby=\"inputGroup-sizing-default\"> "
      + "<button type=\"button\" class=\"btn btn-primary btn-lg btn-block\" onclick='setUserData()'>Login</button>"
      + "  </div>\n"
      + "</div>";
}

function setUserData() {
  var usernameInput = document.querySelector('#usernameInput').value;
  var passwordInput = document.querySelector('#passwordInput').value;
  if (usernameInput && passwordInput) {
    USERNAME = usernameInput;
    PASSWORD = passwordInput;
    auth = "Basic " + btoa(usernameInput + ":" + passwordInput);
    $.ajax({
      type: 'GET',
      url: base_url + "category/all",
      datatype: "html",
      headers: {
        "Authorization": auth
      },
      success: function (data) {
        renderMenu();
      },
      statusCode: {
        401: function () {
          alert("Ошибка! Неверные учетные данные");
          renderAuthForm();
        }
      }
    });
  } else {
    alert("Введите логин и пароль");
  }
}

function renderMenu() {
  mainDiv.innerHTML = "    <div class=\"btn-group-vertical\" style='width: -webkit-fill-available'>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg btn-block\" style='width: -webkit-fill-available; margin: 5px' onclick=\"renderTestMenu()\">Управление тестами</button>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg btn-block\" style='width: -webkit-fill-available; margin: 5px' onclick=\"renderLearningMenu()\">Управление обуччающими материалами</button>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg btn-block\" style='width: -webkit-fill-available; margin: 5px' onclick=\"renderReports()\">Отчетность</button>\n"
      + "      <button type=\"button\" class=\"btn btn-primary btn-lg btn-block\" style='width: -webkit-fill-available; margin: 5px' onclick=\"(function() { location.reload(); })();\">Выход</button>\n"
      + "    </div>";
}

function renderTestMenu() {
  mainDiv.innerHTML = "";
  $.ajax({
    type: 'GET',
    url: base_url + "category/all",
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      mainDiv.innerHTML = "    <div id=\"addTestItemDiv\">\n"
          + "      <button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"renderMenu()\">Back</button>\n"
          + "      <div class=\"card\" style=\"margin: 10px; padding: 5px\">\n"
          + "        <h3>Тест</h3>\n"
          + "        <select id=\"categorySelect\" style=\"margin: 5px; padding: 5px\"></select>\n"
          + "        <input type=\"text\" class=\"form-control\" style=\"margin: 5px; padding: 5px\" placeholder=\"name\" id=\"nameInput\"/>\n"
          + "        <button type=\"button\" class=\"btn btn-primary btn-sm\" style=\"margin: 5px; padding: 5px\" onclick=\"saveTest()\">Save Test</button>\n"
          + "        <h4>Вопросы</h4>\n"
          + "        <h6>Текст задания</h6>\n"
          + "        <input type=\"text\" class=\"form-control\" style=\"margin: 5px; padding: 5px\" placeholder=\"?\" id=\"queryInput\">\n"
          + "        <h6>Варианты ответов</h6>\n"
          + "        <input type=\"text\" class=\"form-control\" style=\"margin: 5px; padding: 5px\" placeholder=\"?\" id=\"answe1_Input\"> \n"
          + "        <input type=\"radio\" name=\"st-type\" id=\"is_right_1\" class=\"btn btn-light\" style=\"margin: 5px\">Верно</label>\n"
          + "        <input type=\"text\" class=\"form-control\" style=\"margin: 5px; padding: 5px\" placeholder=\"?\" id=\"answe2_Input\">\n"
          + "        <input type=\"radio\" name=\"st-type\" id=\"is_right_2\" class=\"btn btn-light\" style=\"margin: 5px\">Верно</label>\n"
          + "        <input type=\"text\" class=\"form-control\" style=\"margin: 5px; padding: 5px\" placeholder=\"?\" id=\"answe3_Input\">\n"
          + "        <input type=\"radio\" name=\"st-type\" id=\"is_right_3\" class=\"btn btn-light\" style=\"margin: 5px\">Верно</label>\n"
          + "        <input type=\"text\" class=\"form-control\" style=\"margin: 5px; padding: 5px\" placeholder=\"?\" id=\"answe4_Input\">\n"
          + "        <input type=\"radio\" name=\"st-type\" id=\"is_right_4\" class=\"btn btn-light\" style=\"margin: 5px\">Верно</label>\n"
          + "        <button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px\" onclick=\"addQuery()\">Add Query</button>\n"
          + "      </div>\n"
          + "    </div>";
      var select = document.querySelector('#categorySelect');
      data.forEach(function (item) {
        select.innerHTML += "<option value='" + item.id +"'>" + item.name + "</option>";
      });

    }
  });
}

function renderLearningMenu() {
  mainDiv.innerHTML = "";
  $.ajax({
    type: 'GET',
    url: base_url + "category/all",
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      mainDiv.innerHTML = "    <div id=\"addLearningItemDiv\">\n"
          + "<button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"renderMenu()\">Back</button>"
          + "      <div class=\"card\" style=\"margin: 10px; padding: 5px\">\n"
          + "        <h3>Добавить статью</h3>\n"
          + "        <select id=\"categorySelect\"></select>\n"
          + "        <input type=\"text\" style='margin: 10px; padding: 10px' class=\"form-control\" placeholder=\"name\" id=\"nameInput\"/>\n"
          + "        <textarea style='margin: 10px; padding: 10px; height: 400px' class=\"form-control\" placeholder=\"\" id=\"itemInput\"></textarea>\n"
          + "        <button type=\"button\" style='margin: 10px; padding: 10px' class=\"btn btn-primary btn-sm\" onclick=\"addLearningItem()\">Add</button>\n"
          + "      </div>\n"
          + "      <div id='addTestItemDivQuery'></div>\n"
          + "    </div>";
      var select = document.querySelector('#categorySelect');
      data.forEach(function (item) {
        select.innerHTML += "<option value='" + item.id +"'>" + item.name + "</option>";
      });

    }
  });
}

function renderReports() {
  mainDiv.innerHTML = "      <button type=\"button\" class=\"btn btn-success btn-sm\" style=\"margin: 5px; padding: 5px; width: 50px\" onclick=\"renderMenu()\">Back</button>\n";
  $.ajax({
    type: 'GET',
    url: base_url + "result/report/all",
    datatype: "application/json",
    async: false,
    headers: {
      "Authorization": auth
    },
    success: function (data) {
      let table = document.createElement('table');
      table.className = "table table-bordered";
      data.forEach(function (user) {
        table.innerHTML +="<tr style='background-color: darkseagreen'><td colspan='3'>" + user.fio + "</td><td>Успеваемость</td><td>" + user.averagePercent + "</td></tr>";
        if (user.results == null ||user.results.length == 0)
          table.innerHTML += "<tr>No results</tr>";
        else {
          table.innerHTML+="<tr><td>Категория</td><td>Тест</td><td>Результат худший</td><td>Результат лучший</td><td>Результат средний</td></tr>";
        }
        user.results.forEach(function (item) {
          let rowUser = document.createElement('tr');
          rowUser.innerHTML +=
              "<td>" + item.category + "</td>"
              + "<td>" + item.name + "</td>"
              + "<td>" + item.worstPercent.toFixed(2) + "%</td>"
              + "<td>" + item.bestPercent.toFixed(2) + "%</td>"
              + "<td>" + item.averagePercent.toFixed(2) + "%</td>";
          table.appendChild(rowUser);
        });
      });

      mainDiv.appendChild(table);
    }
  });
}

function addLearningItem() {
  var item = {
    name : document.querySelector('#nameInput').value.toString(),
    item: document.querySelector('#itemInput').value.toString(),
    category_id: document.querySelector('#categorySelect').value.toString()
  };
  $.ajax({
    type: 'POST',
    url: base_url + "learningItem/save",
    datatype: "application/json",
    headers: {
      "Authorization": auth,
      "Content-type": "application/json"
    },
    data: JSON.stringify(item),
    success: function (data) {
      alert("Сохранено");
      renderLearningMenu();
    }
  });
}

function saveTest() {

  var item = {
    name : document.querySelector('#nameInput').value.toString(),
    queryList: queryList,
    category_id: document.querySelector('#categorySelect').value.toString()
  };
  $.ajax({
    type: 'POST',
    url: base_url + "test/save",
    datatype: "application/json",
    headers: {
      "Authorization": auth,
      "Content-type": "application/json"
    },
    data: JSON.stringify(item),
    success: function (data) {
      alert("Сохранено");
      renderMenu();
      queryList = [];
    },
    error: function () {
      alert("error! Некорректно заполненные данные!");
    }
  });

}

function addQuery() {
  let queryInput= document.querySelector('#queryInput').value;

  let answer1 = document.querySelector('#answe1_Input').value;
  let answer2 = document.querySelector('#answe2_Input').value;
  let answer3 = document.querySelector('#answe3_Input').value;
  let answer4 = document.querySelector('#answe4_Input').value;

  let numOfRight = 0;
  if (document.querySelector('#is_right_1').checked) numOfRight = 1;
  if (document.querySelector('#is_right_2').checked) numOfRight = 2;
  if (document.querySelector('#is_right_3').checked) numOfRight = 3;
  if (document.querySelector('#is_right_4').checked) numOfRight = 4;

  let query = {
    query: queryInput,
    numOfRight: numOfRight,
    answer1: answer1,
    answer2: answer2,
    answer3: answer3,
    answer4: answer4
  };
  queryList.push(query);

  document.querySelector('#queryInput').value = "";
  document.querySelector('#answe1_Input').value = "";
  document.querySelector('#answe2_Input').value = "";
  document.querySelector('#answe3_Input').value = "";
  document.querySelector('#answe4_Input').value = "";

}

