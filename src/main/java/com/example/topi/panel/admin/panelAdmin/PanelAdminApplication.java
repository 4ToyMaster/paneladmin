package com.example.topi.panel.admin.panelAdmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanelAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanelAdminApplication.class, args);
	}

}

